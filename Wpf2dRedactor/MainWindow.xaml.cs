﻿using System;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Wpf2dRedactor.Model;

namespace Wpf2dRedactor
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Point _Start;
        Point _End;
        EnumShape curShape;
        DrawingBoard DrBoard;
        bool IsMove = false;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Random r = new Random();

            DrBoard = new DrawingBoard();
            DFCanvas.DataContext = DrBoard;

            DrBoard.Items.CollectionChanged += UpdateBoard;
            curShape = EnumShape.Line;
        }

        private void DFCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _Start = e.GetPosition(this);
        }

        private void DFCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            //_End = e.GetPosition(this);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                _End = e.GetPosition(this);
                if (IsMove) DrBoard.MoveItem(e.GetPosition(this));
            }
        }

        private void DFCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DFCanvas.Focus();
            if (!IsMove) DrBoard.CreateItem(curShape, _Start, _End);
        }

        private void UpdateBoard(object o, NotifyCollectionChangedEventArgs oo)
        {
            if (oo.Action == NotifyCollectionChangedAction.Remove)
                foreach (var item in oo.OldItems)
                    DFCanvas.Children.Remove((item as Item).Shape);
            if (oo.Action == NotifyCollectionChangedAction.Add)
                foreach (var item in oo.NewItems)
                    DFCanvas.Children.Add((item as Item).Shape);
            if (oo.Action == NotifyCollectionChangedAction.Reset)
                DFCanvas.Children.Clear();
        }

        private void btReact_Click(object sender, RoutedEventArgs e)
        {
            IsMove = false;
            curShape = EnumShape.Rectangle;
        }

        private void btLine_Click(object sender, RoutedEventArgs e)
        {
            IsMove = false;
            curShape = EnumShape.Line;
        }

        private void DF_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DrBoard.DeleteItem();
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Delete)
            //    DrBoard.DeleteItem();
        }

        private void isMove_Click(object sender, RoutedEventArgs e)
        {
            IsMove = true;
        }

        private void DFCanvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            PointHitTestResult hitTestResult = (PointHitTestResult)VisualTreeHelper.HitTest(DFCanvas, e.GetPosition(this));
            DrBoard.Select(hitTestResult.VisualHit as Shape);
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            DrBoard.Clear();
        }
        private void gPalet_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DrBoard.SelectedItem != null)
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    DrBoard.CurentStrocke = (e.Source as Button).Background;
                    DrBoard.SelectedItem.StrokeChange((e.Source as Button).Background);
                }
                if (e.RightButton == MouseButtonState.Pressed)
                {
                    DrBoard.CurentFill = (e.Source as Button).Background;
                    DrBoard.SelectedItem.FillChange((e.Source as Button).Background);
                }
            }
        }
        private void tbRotate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                if (DrBoard?.SelectedItem != null)
                    DrBoard.SelectedItem.Rotate(int.Parse(string.IsNullOrEmpty(tbRotate.Text) ? "0" : tbRotate.Text));
        }

        private void tbThickness_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                if (DrBoard?.SelectedItem != null)
                    DrBoard.SelectedItem.ThicknessChange(int.Parse(string.IsNullOrEmpty(tbThickness.Text) ? "0" : tbThickness.Text));
        }
    }
}
