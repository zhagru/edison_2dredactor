﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Wpf2dRedactor.Model
{
    public abstract class Item
    {
        public Shape Shape;
        public virtual void Edit(params Point[] param)
        {

        }
        public virtual void Rotate(int index)
        {
                System.Windows.Media.Matrix m = Shape.RenderTransform.Value;
                System.Windows.Rect r = Shape.RenderedGeometry.Bounds;
                m.Translate(-r.Left - r.Width / 2, -r.Top - r.Height / 2);
                m.Rotate(index);
                m.Translate(r.Left + r.Width / 2, r.Top + r.Height / 2);
                Shape.RenderTransform = new MatrixTransform(m);
        }
        public virtual void FillChange(Brush br)
        {
            if (br != null)
                Shape.Fill = br;
        }
        public virtual void StrokeChange(Brush br)
        {
            if (br != null)
                Shape.Stroke = br;
        }
        public virtual void ThicknessChange(int index)
        {
            Shape.StrokeThickness = index;
        }
    }
}
