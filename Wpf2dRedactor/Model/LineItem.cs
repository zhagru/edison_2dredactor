﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Wpf2dRedactor.Model
{
    public class LineItem : Item
    {
        public LineItem(Point start, Point end, Brush stroke, Brush fill, int thick = 5)
        {
            Shape = new Polyline
            {
                Stroke = stroke,
                Fill = fill,
                StrokeThickness = thick
            };
            (Shape as Polyline).Points = new PointCollection(new Point[] { Point.Subtract(start, new Vector(65, 0)), Point.Subtract(end, new Vector(65,0))});
        }
        public LineItem(Shape sh)
        {
            Shape = sh;
        }
        public override void Edit(params Point[] param)
        {
            (Shape as Polyline).Points = new PointCollection(param);
        }
    }
}
