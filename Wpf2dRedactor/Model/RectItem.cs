﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Wpf2dRedactor.Model
{
    public class RectItem : Item
    {
        public RectItem(Point start, Point end, Brush stroke, Brush fill, int thick = 5)
        {
            Shape = new Rectangle
            {
                Stroke = stroke,
                Fill = fill,
                StrokeThickness = thick
            };
            if (end.X >= start.X)
            {
                // Defines the left part of the rectangle
                Shape.SetValue(Canvas.LeftProperty, start.X - 65);
                Shape.Width = end.X - start.X;
            }
            else
            {
                Shape.SetValue(Canvas.LeftProperty, end.X - 65);
                Shape.Width = start.X - end.X;
            }

            if (end.Y >= start.Y)
            {
                // Defines the top part of the rectangle
                Shape.SetValue(Canvas.TopProperty, start.Y);
                Shape.Height = end.Y - start.Y;
            }
            else
            {
                Shape.SetValue(Canvas.TopProperty, end.Y);
                Shape.Height = start.Y - end.Y;
            }
        }
        public RectItem(Shape sh)
        {
            Shape = sh;
        }
    }
}
