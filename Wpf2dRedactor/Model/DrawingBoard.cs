﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Wpf2dRedactor.Model
{
    public class DrawingBoard
    {
        public ObservableCollection<Item> Items { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int CurentThikness { get; set; }
        public Item SelectedItem { get; set; }
        public Brush CurentFill { get; set; }
        public Brush CurentStrocke {get;set;}
        public DrawingBoard() 
        {
            CurentThikness = 2;
            Items = new ObservableCollection<Item>();
            Width = 1000;
            Height = 1000;
            CurentStrocke = Brushes.Black;
            CurentFill = Brushes.White;
        }
        public DrawingBoard(int width = 1000, int height = 1000)
        {
            CurentThikness = 2;
            Width = width;
            Height = height;
            Items = new ObservableCollection<Item>();
        }

        public void CreateItem(EnumShape type, Point startPosition, Point stopPosition)
        {
            if (type == EnumShape.Line)
                Items.Add(new LineItem(startPosition, stopPosition, CurentStrocke, CurentFill, CurentThikness));
            if (type == EnumShape.Rectangle)
                Items.Add(new RectItem(startPosition, stopPosition, CurentStrocke, CurentFill, CurentThikness));
            SelectedItem = Items.LastOrDefault();
        }
        public void DeleteItem()
        {
            if (SelectedItem is Item)
                Items.Remove(SelectedItem);
            SelectedItem = Items.LastOrDefault();
        }
        public void MoveItem(Point end)
        {
            Item item = SelectedItem;
            Shape shape = item?.Shape;

            shape?.SetValue(Canvas.TopProperty, end.Y);
            shape?.SetValue(Canvas.LeftProperty, end.X - 65);

            if (item is LineItem)
            {
                Point endLine = (shape as Polyline).Points.Last();
                shape?.SetValue(Canvas.BottomProperty, end.Y - endLine.Y);
                shape?.SetValue(Canvas.RightProperty, end.X - 65 - endLine.X);
            }
        }
        public void Select(Shape elem)
        {
            SelectedItem = Items.Where(x => x.Shape == elem).FirstOrDefault();
        }
        public void Clear()
        {
            Items.Clear();
        }
        public void Save(string path)
        {
            throw new NotImplementedException();
        }
        public void Load(string path)
        {
            throw new NotImplementedException();
        }
    }
}
